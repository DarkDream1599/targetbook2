from django.db import models
from django.contrib.auth.models import User as AuthUser
from django.core.validators import MaxValueValidator, MinValueValidator

from .managers.models_managers import TimeManager
from .managers.models_managers import TargetManager
from .managers.models_managers import StepManager
from .managers.models_managers import HumidityManager
from .managers.models_managers import AirSaturationManager

from .managers.models_managers import MAX_TITLE


class Time(models.Model):
    days = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    hours = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(23)])
    minutes = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(59)])

    objects = TimeManager()


class Location(models.Model):
    ENGLISH = "ENG"
    UKRAINIAN = "UKR"

    LOCATION_CHOICES = (
        (ENGLISH, 'ENG'),
        (UKRAINIAN, 'UKR'),
    )

    title = models.CharField(
        max_length=MAX_TITLE,
        choices=LOCATION_CHOICES,
        default=ENGLISH,
        unique=True
    )


class Target(models.Model):
    title = models.CharField(max_length=MAX_TITLE, default="")
    description = models.TextField(default="")

    objects = TargetManager()


class Step(models.Model):
    title = models.CharField(max_length=MAX_TITLE)
    priority = models.IntegerField(validators=[MinValueValidator(0)])
    description = models.TextField(default="")
    time = models.OneToOneField(Time, null=True, on_delete=models.SET_NULL, primary_key=False)
    target = models.ForeignKey(Target, default=None, null=True, on_delete=models.CASCADE)

    objects = StepManager()


class User(models.Model):
    location = models.ForeignKey(Location, default=None, null=True, on_delete=models.PROTECT)
    auth_user = models.OneToOneField(AuthUser, default=None, on_delete=models.CASCADE)
    mac_address = models.CharField(max_length=MAX_TITLE, default="")


class Statistic(models.Model):
    pass


class Humidity(models.Model):
    value = models.FloatField()
    statistic = models.ForeignKey(Statistic, default=None, on_delete=models.CASCADE)
    time = models.ForeignKey(Time, default=None, null=True, on_delete=models.SET_DEFAULT)
    objects = HumidityManager()


class Temperature(models.Model):
    value = models.IntegerField()
    statistic = models.ForeignKey(Statistic, default=None, on_delete=models.CASCADE)
    time = models.ForeignKey(Time, default=None, null=True, on_delete=models.SET_DEFAULT)


class AirSaturation(models.Model):
    value = models.FloatField(validators=[MinValueValidator(0), MaxValueValidator(100)])
    statistic = models.ForeignKey(Statistic, default=None, on_delete=models.CASCADE)
    time = models.ForeignKey(Time, default=None, null=True, on_delete=models.SET_DEFAULT)
    objects = AirSaturationManager()


class Plans(models.Model):
    time = models.OneToOneField(Time, null=True, on_delete=models.SET_NULL, primary_key=False)
    target = models.OneToOneField(Target, null=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(User, default=None, on_delete=models.CASCADE)
    active = models.BooleanField(default=False)
    statistic = models.OneToOneField(Statistic, default=None, null=True, on_delete=models.SET_NULL)


def load_locations():
    Location.objects.create(title=Location.ENGLISH)
    Location.objects.create(title=Location.UKRAINIAN)


def load_initial_data():
    load_locations()

