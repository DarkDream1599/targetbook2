from django.db import models

from targetapp.managers.field_managers import LimitedFieldNumberWrapper
from targetapp.managers.field_managers import LimitedFieldLengthWrapper


MAX_TITLE = 30


class StepManager(models.Manager):
    @LimitedFieldLengthWrapper(field='title', max_len=MAX_TITLE)
    @LimitedFieldNumberWrapper(field='priority', min_value=1, max_value=1e5)
    def create(self, **obj_data):
        return super().create(**obj_data)


class HumidityManager(models.Manager):
    @LimitedFieldNumberWrapper(field='value', min_value=0, max_value=100)
    def create(self, **obj_data):
        return super().create(**obj_data)


class AirSaturationManager(models.Manager):
    @LimitedFieldNumberWrapper(field='value', min_value=0, max_value=100)
    def create(self, **obj_data):
        return super().create(**obj_data)


class TimeManager(models.Manager):
    @LimitedFieldNumberWrapper(field='minutes', min_value=0, max_value=59)
    @LimitedFieldNumberWrapper(field='hours', min_value=0, max_value=23)
    @LimitedFieldNumberWrapper(field='days', min_value=0, max_value=32850)
    def create(self, **obj_data):
        return super().create(**obj_data)


class TargetManager(models.Manager):
    @LimitedFieldLengthWrapper(field='title', max_len=MAX_TITLE)
    def create(self, **obj_data):
        return super().create(**obj_data)

