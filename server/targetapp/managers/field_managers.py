from functools import wraps


class LimitedFieldLengthWrapper:
    def __init__(self, field, max_len):
        self.field = field
        self.max_length = max_len

    def __call__(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if self.field in kwargs:
                if len(kwargs[self.field]) > self.max_length:
                    raise ValueError(f"Length of {self.field} must be shorter {self.max_length} characters")

            return func(*args, **kwargs)
        return wrapper


class LimitedFieldNumberWrapper:
    def __init__(self, field, min_value, max_value):
        self.field = field
        self.min_value = min_value
        self.max_value = max_value

    def __call__(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if self.field in kwargs:
                if int(kwargs[self.field]) < self.min_value or int(kwargs[self.field]) > self.max_value:
                    raise ValueError(f"{self.field} must be between {self.min_value} ... {self.max_value}")

            return func(*args, **kwargs)
        return wrapper
